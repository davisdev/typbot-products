# Running tests
1. There are few ways: `php artisan test` (normal, old way), `php artisan test --parallel` (parallel testing). No other way I run them personally.

# Some side-notes
1. By running `php artisan db:seed` you will seed products for random users. To seed products for your registered & authenticated user, run `php artisan seed-products <userID>` where `<userID>` should be replaced with user ID you are currently signed-in with.
You might ask - well, how do I know it? On home page there will be notice on what to run if there are no products and ID will be populated.
2. Since there was no need for product attribute CRUD, products with attributes are **only** created with `php artisan seed-products <userID>` (you can also pass `--count=<number of products>`).
3. API routes will be listed on home page (no API key required, no need for additional auth since API and web interface share the same guard).

# Thoughts on this & suggestion if needed refactoring (but outside the scope of the project)
1. Products should be cached since they won't change a lot in this case. Would go for Redis caching where I would do cache busting on new product creation/update.
2. Of course, visual product page should be paginated as the product list grows.
3. There could've been done some additional testing to cover all the "unhappy" paths when it comes to validation.
4. If there were attributes displayed on web, we should eager load attributes along with products since they would almost always go hand in hand.
5. Validation rules could be reused for same resource create/update operations.
6. Since not a lot of modification is happening in resource classes, we could just spit out models by just straight up returning them from controllers. Objects get casted to JSON in responses either way.
7. Commands are "auto-loaded" in command registry (enabled by default), but I like to be explicit on what commands this project has on its own.
8. Foreign key casting to int should be done more seemingly, for now - exclusively casted in model itself.
9. Views could be refactored to multiple includes/components, but that's outside the scope.
10. New features could've been made by following principle of "1 feature = 1 branch", but that would be waste of time on such a small project.
