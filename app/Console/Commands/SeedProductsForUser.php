<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class SeedProductsForUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "seed-products {user : ID of the user for which to seed products} {--count= : Amount of products to seed}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeds products along with attributes for user';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user = (int) $this->argument('user');
        $count = (int) $this->option('count') ?: 20;

        Product::factory()
            ->count($count)
            ->hasAttributes(3)
            ->create(['user_id' => $user]);

        $this->info("{$count} products (with attributes) successfully seeded!");

        return 0;
    }
}
