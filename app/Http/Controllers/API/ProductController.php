<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductResourceCollection;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductController extends Controller
{
    public function index(): JsonResource
    {
        return ProductResourceCollection::make(
            auth()->user()->products()->paginate(15),
        );
    }

    public function show(Product $product): JsonResource
    {
        return ProductResource::make($product->load('attributes'));
    }
}
