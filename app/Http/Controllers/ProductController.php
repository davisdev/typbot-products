<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('product.index', [
            'products' => auth()->user()->products()->latest()->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProductRequest $request
     * @return RedirectResponse
     */
    public function store(StoreProductRequest $request): RedirectResponse
    {
        auth()->user()->products()->create($request->validated());

        session()->flash('success', 'Product successfully created!');

        return redirect(route('product.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return View
     */
    public function show(Product $product): View
    {
        return view('product.show', [
            'product'           => $product,
            'productAttributes' => [],
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return View
     */
    public function edit(Product $product): View
    {
        abort_if(! $product->isOwnedBy(auth()->id()), 403, 'Editing not owned products is forbidden!');

        return view('product.edit', [
            'product' => $product,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @param Product $product
     * @return RedirectResponse
     */
    public function update(UpdateProductRequest $request, Product $product): RedirectResponse
    {
        abort_if(! $product->isOwnedBy(auth()->id()), 403, 'Updating not owned products is forbidden!');

        $product->update($request->validated());

        session()->flash('success', 'Product successfully updated!');

        return redirect(route('product.show', ['product' => $product]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return RedirectResponse
     */
    public function destroy(Product $product): RedirectResponse
    {
        abort_if(! $product->isOwnedBy(auth()->id()), 403, 'Deleting not owned products is forbidden!');

        $product->delete();

        session()->flash('success', 'Product successfully deleted!');

        return redirect(route('product.index'));
    }
}
