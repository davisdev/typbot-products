<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'        => 'required|min:3|max:255|unique:products,name',
            'description' => 'nullable|min:3|max:500',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required'   => 'Product name is a required field',
            'name.min'        => 'Product name must be at least :min characters long',
            'name.max'        => 'Product name must not be longer than :max characters',
            'name.unique'     => 'Product name must be unique',
            'description.min' => 'Product description must be at least :min characters long',
            'description.max' => 'Product description must not be longer than :max characters',
        ];
    }
}
