<?php

namespace Tests\Feature\E2E;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    private User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            $this->user = User::factory()->create()
        );
    }

    /** @test */
    public function it_shows_product_list_page()
    {
        $products = Product::factory()->count(3)->create(['user_id' => $this->user->id]);

        $this->get(route('product.index'))
            ->assertSuccessful()
            ->assertSee([
                ...$products->pluck('name'),
                'Show product',
            ]);
    }

    /** @test */
    public function it_shows_product_creation_page()
    {
        $this->get(route('product.create'))
            ->assertSuccessful()
            ->assertSee(['Create a Product', 'Create']);
    }

    /** @test */
    public function it_can_successfully_store_a_product()
    {
        $productData = Product::factory()->make(['user_id' => $this->user->id])->toArray();

        $response = $this
            ->post(route('product.store'), $productData)
            ->assertSessionHasNoErrors()
            ->assertSessionHas('success', 'Product successfully created!')
            ->assertRedirect(route('product.index'));

        $this->assertDatabaseHas('products', $productData);

        $this->followRedirects($response)
            ->assertSee([
                $productData['name'],
                $productData['description'],
            ]);
    }

    /** @test */
    public function it_shows_page_containing_data_about_product()
    {
        $product = Product::factory()->create(['user_id' => $this->user->id]);

        $this->get(route('product.show', ['product' => $product]))
            ->assertSuccessful()
            ->assertSee([
                $product->name,
                $product->description,
            ]);
    }

    /** @test */
    public function it_shows_page_for_editing_a_product()
    {
        $product = Product::factory()->create(['user_id' => $this->user->id]);

        $this->get(route('product.edit', ['product' => $product]))
            ->assertSuccessful()
            ->assertSee([
                'Edit a Product',
                $product->name,
                $product->description,
                'Save',
            ]);
    }

    /** @test */
    public function it_can_successfully_update_the_product()
    {
        $product = Product::factory()->create(['user_id' => $this->user->id]);

        $response = $this->patch(route('product.update', ['product' => $product]), [
            'name'        => $updatedName = 'Much better name',
            'description' => $updatedDescription = 'More interesting description',
        ])
            ->assertSessionHasNoErrors()
            ->assertSessionHas('success', 'Product successfully updated!')
            ->assertRedirect(route('product.show', ['product' => $product->fresh()]));

        $this->assertDatabaseHas('products', [
            'name'        => $updatedName,
            'description' => $updatedDescription,
        ]);

        $this->followRedirects($response)
            ->assertSee([
                "Product details",
                $updatedName,
                $updatedDescription,
            ]);
    }

    /**
     * @test
     * @dataProvider validationFailingAttributes
     */
    public function product_store_operation_fails_with_given_values($name, $description, $failingField)
    {
        $data = [
            'name'        => $name,
            'description' => $description,
        ];

        if ($description === null) {
            // Due to description not being sent in,
            // we got to simulate it being under "nullable" validation rule.
            unset($data['description']);
        }

        $this->post(route('product.store'), $data)
            ->assertSessionHasErrors($failingField);
    }

    /**
     * @test
     * @dataProvider validationFailingAttributes
     */
    public function product_update_operation_fails_with_given_values($name, $description, $failingField)
    {
        $product = Product::factory()->create(['user_id' => $this->user->id]);

        $this->patch(route('product.update', ['product' => $product]), [
            'name'        => $name,
            'description' => $description,
        ])
            ->assertSessionHasErrors($failingField);
    }

    public function validationFailingAttributes(): array
    {
        return [
            ['Te', 'Te', ['name', 'description']],
            [Str::repeat('a', 256), 'Test', 'name'],
            ['Test', Str::repeat('a', 501), 'description'],
            [null, 'Test', 'name'],
        ];
    }

    /** @test */
    public function product_update_operation_fails_when_different_product_owner_tries_to_update_your_product()
    {
        $product = Product::factory()->create(['user_id' => $this->user->id]);

        $this->actingAs(User::factory()->create())
            ->patch(route('product.update', ['product' => $product]), [
                'name'        => 'Test title',
                'description' => 'Test description',
            ])
            ->assertForbidden();
    }

    /** @test */
    public function product_delete_operation_fails_when_different_product_owner_tries_to_update_your_product()
    {
        $product = Product::factory()->create(['user_id' => $this->user->id]);

        $this->actingAs(User::factory()->create())
            ->delete(route('product.destroy', ['product' => $product]))
            ->assertForbidden();
    }

    /** @test */
    public function it_shows_json_response_with_all_user_products()
    {
        $products = Product::factory()->count(10)->create(['user_id' => $this->user->id]);
        $otherProducts = Product::factory()->count(5)->create();

        $this->getJson(route('api.product.index'))
            ->assertSuccessful()
            ->assertJson([
                'data' => $products->only(['name', 'description'])->toArray(),
            ])
            ->assertJsonMissing([
                $otherProducts->only(['name', 'description'])->toArray(),
            ]);
    }

    /** @test */
    public function it_shows_product_with_all_its_attributes()
    {
        $product = Product::factory()
            ->hasAttributes(3)
            ->create(['user_id' => $this->user->id]);

        $this->getJson(route('api.product.show', ['product' => $product]))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    'name',
                    'description',
                    'attributes' => [
                        [
                            'key',
                            'value',
                        ],
                    ]
                ]
            ]);
    }
}
