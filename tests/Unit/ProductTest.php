<?php

namespace Tests\Unit;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    private User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            $this->user = User::factory()->create()
        );
    }

    /** @test */
    public function it_can_create_product_with_specified_values_for_specified_user()
    {
        $this->user->products()->create($attributes = [
            'name'        => 'Brand new TV',
            'description' => 'Your gateway for watching Discovery channel',
        ]);

        $this->assertInstanceOf(Product::class, $this->user->products()->first());
        $this->assertDatabaseHas('products', $attributes);
    }

    /** @test */
    public function product_can_be_soft_deleted()
    {
        $product = Product::factory()->create(['user_id' => $this->user->id]);

        $this->assertDatabaseHas('products', $product->only(['name', 'description']));
        $product->delete();
        $this->assertDatabaseHas('products', $product->only(['name', 'description']));
        $this->assertNotNull($product->deleted_at);
    }

    /** @test */
    public function it_can_show_product_url()
    {
        $product = Product::factory()->create();

        $this->assertEquals("http://localhost/products/{$product->id}", $product->url());
    }

    /** @test */
    public function it_properly_checks_if_product_belongs_to_specific_user()
    {
        /** @var Product $ownProduct */
        $ownProduct = Product::factory()->create(['user_id' => $this->user->id]);
        // Creates product for random user that's not $this->user assigned user instance
        $notOwnProduct = Product::factory()->create();

        $this->assertTrue($ownProduct->isOwnedBy($this->user->id));
        $this->assertFalse($notOwnProduct->isOwnedBy($this->user->id));
    }
}
