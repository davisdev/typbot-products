<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Create a Product
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('product.store') }}" method="post">
                        @csrf

                        <div class="input-group">
                            <label for="name">Product Name</label>
                            <input
                                type="text"
                                id="name"
                                name="name"
                                placeholder="Enter product name..."
                                class="input"
                                value="{{ old('name') }}"
                            >
                            @error('name')
                                <span class="error">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="input-group">
                            <label for="description">Description</label>
                            <textarea
                                id="description"
                                name="description"
                                placeholder="Enter product description..."
                                rows="15"
                                cols="50"
                                class="input"
                            >{{ old('description') }}</textarea>
                            @error('description')
                                <span class="error">{{ $message }}</span>
                            @enderror
                        </div>

                        <button type="submit" class="button button-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
