<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            All Products
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @include('partials._notifications')

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="product-list grid grid-cols-4 gap-5">
                        @forelse($products as $product)
                            @include('partials._product-tile')
                        @empty
                            There are no products. <a class="hover:underline font-semibold text-blue-500" href="{{ route('product.create') }}">Create one?</a>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
