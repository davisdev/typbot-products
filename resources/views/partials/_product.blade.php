<div class="product flex flex-col">
    <div class="details mb-5">
        <h1 class="text-2xl font-bold mb-5">{{ $product->name }}</h1>
        <div>
            <h1 class="font-bold">Description</h1>
            <p class="text-gray-900 opacity-90">{{ $product->description }}</p>
        </div>
    </div>

    <div class="actions flex w-1/2 justify-between">
        <a href="{{ route('product.edit', ['product' => $product]) }}" class="button button-info">Edit</a>
        <form action="{{ route('product.destroy', ['product' => $product]) }}" method="post">
            @csrf
            @method('DELETE')

            <button type="submit" class="button button-danger"
                    onclick="return window.confirm('Do you really want to delete the product?')">Delete
            </button>
        </form>
    </div>

</div>
