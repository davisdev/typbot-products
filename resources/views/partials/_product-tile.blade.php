<div class="product flex flex-col justify-between px-5 py-2 mb-5 shadow-md">
    <h1 class="text-xl mb-5">{{ $product->name }}</h1>

    <div class="mb-5">
        <h1 class="font-bold">Description</h1>
        <p class="text-sm">{{ $product->description }}</p>
    </div>

    <a href="{{ $product->url() }}" class="text-blue-500 font-bold hover:underline">Show product</a>
</div>
