@if(session()->has('success'))
    <div class="success-notification text-gray-100 py-2 px-5 bg-green-500 rounded mb-5">
        <span>{{ session()->get('success') }}</span>
    </div>
@endif
